FROM "python:3"

RUN mkdir /code
WORKDIR /code

RUN apt-get update && apt-get -y install \
    apt-transport-https \
    unixodbc-dev

RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl http://packages.microsoft.com/config/debian/9/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update && ACCEPT_EULA=Y apt-get -y install msodbcsql17

COPY requirements.txt /code/
RUN pip install -r requirements.txt

COPY . /code/
