# django-mssql

sample project to show configuration of connecting Django to MSSQL (tsql), with docker or virtual environment

## docker

1. create `.env` file ([see sample file](./.env.example))
2. run the containers using `docker-compose up -d`

## virtual environment

1. create virtual environment `python -m venv .pyenv` and activate it `source .pyenv/scripts/activate`
2. install requirements `python -m pip install -r requirements.txt`
3. remove `options` from the `miner_db` in [django_mssql/settings.py](django_mssql/settings.py)
4. run dev server `python manage.py runserver 127.0.0.1:8000`
