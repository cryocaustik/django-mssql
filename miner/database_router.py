

class MinerRouter:

    def db_for_read(self, model, **hints):
        """
        Attempts to read miner models go to express.
        """
        if model._meta.app_label == 'miner':
            return 'miner_db'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write miner models go to express.
        """
        if model._meta.app_label == 'miner':
            return 'miner_db'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the miner app is involved.
        """
        if obj1._meta.app_label == 'miner' or \
           obj2._meta.app_label == 'miner':
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the miner app only appears in the 'miner_db'
        database.
        """
        if app_label == 'miner':
            return db == 'miner_db'
        return None