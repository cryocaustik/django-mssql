from django.views import View
from django.shortcuts import render, HttpResponse, redirect
from miner.models import Post

# Create your views here.
class PostView(View):

    def get(self, request):
        posts = Post.objects.all()
        return render(request, 'miner/posts.html', {'posts': posts})

    def post(self, request):
        title = request.POST.get('title')
        content = request.POST.get('content')
        if not all([title, content]):
            raise ValueError(f'missing fields :{[title, content]}')
        
        post = Post(
            title=title,
            content=content,
            created_by_id=1,
            created_by='admin'
        )
        post.save()
        return redirect('posts')
